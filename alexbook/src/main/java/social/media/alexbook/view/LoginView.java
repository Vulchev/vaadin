package social.media.alexbook.view;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

import social.media.alexbook.service.AccountService;
import social.media.alexbook.view.custom.component.ForgotPasswordPopUp;

@PageTitle("Login")
@Route(value = "login", layout = MainLayout.class)
@PWA(name = "Login form", shortName = "login")
public class LoginView extends VerticalLayout {

	private LoginForm loginForm;
	private Anchor registrationLink;
	private ForgotPasswordPopUp popUp;

	private Button test = new Button("Test params");

	public LoginView(@Autowired AccountService accountService) {
//		test.addClickListener(click -> {
//			this.getUI().ifPresent(ui -> ui.navigate(GreetingComponent.class, "sperma"));
//		});
		popUp = new ForgotPasswordPopUp(accountService);
		loginForm = new LoginForm();
		loginForm.addLoginListener(event -> {
			if (accountService.checkUserCredentials(event.getUsername(), event.getPassword()) != null) {
				this.getUI().ifPresent(nav -> nav.navigate(GreetingComponent.class, true));
			} else {
				loginForm.setError(true);
			}
		});
		registrationLink = new Anchor("http://localhost:8080/register", "Register");
		loginForm.addForgotPasswordListener(event -> {
			popUp.open();
		});
		setHorizontalComponentAlignment(Alignment.CENTER, loginForm, registrationLink);
		add(test, loginForm, registrationLink, popUp);
	}

}
