package social.media.alexbook.view;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;

@Route(value = "greet")
public class GreetingComponent extends Div implements HasUrlParameter<Boolean> {

	@Override
	public void setParameter(BeforeEvent event, @OptionalParameter Boolean parameter) {
		if (parameter == null) {
			setText("You need to be logged in to use the site");
		} else {
			setText(String.format("Welcome %s.", parameter));
		}
	}
}
