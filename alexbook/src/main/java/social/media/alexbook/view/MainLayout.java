package social.media.alexbook.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;

public class MainLayout extends AppLayout implements RouterLayout {

	public MainLayout() {
		setPrimarySection(AppLayout.Section.NAVBAR);
		Tabs tabs = new Tabs(createMenuTab("Login", VaadinIcon.PLUS_SQUARE_O, LoginView.class),
				createMenuTab("Register", VaadinIcon.MINUS, RegisterView.class));
		tabs.setOrientation(Tabs.Orientation.VERTICAL);
		MenuBar menuBar = new MenuBar();
		menuBar.setOpenOnHover(true);
		MenuItem project = menuBar.addItem("Project");
		MenuItem account = menuBar.addItem("Account");
		menuBar.addItem("Sign out", event -> {
			UI.getCurrent().navigate(LoginView.class);
		});
		SubMenu projectSubMenu = project.getSubMenu();
		MenuItem users = projectSubMenu.addItem("Users");
		MenuItem billing = projectSubMenu.addItem("Billing");

		SubMenu usersSubMenu = users.getSubMenu();
		MenuItem test = usersSubMenu.addItem("test");
		SubMenu accountSubMenu = account.getSubMenu();
		MenuItem list = accountSubMenu.addItem("List");
		MenuItem add = accountSubMenu.addItem("Add");
		DrawerToggle toggle = new DrawerToggle();
		toggle.clickInClient();

		addToNavbar(toggle, menuBar);
		addToDrawer(tabs);
	}

	private Tab createMenuTab(String title, VaadinIcon icon, Class<? extends Component> target) {
		RouterLink link = new RouterLink(null, target);
		link.add(title);
		if (icon != null) {
			link.add(icon.create());
		}
		Tab tab = new Tab();
		tab.add(link);
		return tab;
	}

}
