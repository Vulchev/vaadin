package social.media.alexbook.view.custom.component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

import social.media.alexbook.service.AccountService;

public class ForgotPasswordPopUp extends Dialog {

	private AccountService accountService;

	private TextField email = new TextField();
	private Button retrievePassword = new Button("Retrieve", VaadinIcon.KEY.create());
	private TextField password = new TextField();

	public ForgotPasswordPopUp(AccountService accountService) {
		this.accountService = accountService;
		password.setReadOnly(true);
		retrievePassword.addClickListener(click -> password.setValue(retrievePass(email.getValue())));
		VerticalLayout layout = new VerticalLayout();
		email.setPlaceholder("Your email");
		layout.add(email, password, retrievePassword);
		layout.setHorizontalComponentAlignment(Alignment.CENTER, retrievePassword);
		add(layout);

	}

	private String retrievePass(String email) {
		return accountService.retrievePass(email);
	}

}
