package social.media.alexbook.view;

import java.time.LocalDate;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import social.media.alexbook.enums.Gender;
import social.media.alexbook.model.entities.Account;
import social.media.alexbook.service.AccountService;

@PageTitle("Register")
@Route(value = "register", layout = MainLayout.class)
public class RegisterView extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private transient AccountService accountService;

	private FormLayout formLayout;
	private TextField userName = new TextField("User name");
	private PasswordField password = new PasswordField("Password");
	private EmailField email = new EmailField("Email");
	private DatePicker birthDate = new DatePicker("Birth date");
	private ComboBox<Gender> gender = new ComboBox<>("Gender");
	private Button saveButton = new Button("Save", VaadinIcon.PLUS_CIRCLE.create());
	private Button resetButton = new Button("Reset", VaadinIcon.MINUS_CIRCLE.create());
	private H3 title = new H3("Register");
	private Paragraph description = new Paragraph();
	private Anchor linkToLoginView = new Anchor("http://localhost:8080/login", "Go back to login!");
	private Binder<Account> binder = new Binder<>(Account.class);

	public RegisterView() {

		saveButton.addClickListener(click -> saveNewAccount());
		resetButton.addClickListener(click -> resetRegistrationForm());

		HorizontalLayout buttonsLayout = new HorizontalLayout(saveButton, resetButton);
		description.setText(
				"Please fill in the mandatory fields of the registration form! If you clicked register by mistake"
						+ "you can go back to login page with the link below");
		formLayout = new FormLayout();
		userName.setRequired(true);
		userName.focus();
		password.setRequired(true);
		email.setRequiredIndicatorVisible(true);
		gender.setItems(Gender.values());
		formLayout.add(userName, password, email, birthDate, gender);
		setHorizontalComponentAlignment(Alignment.CENTER, formLayout, buttonsLayout, title, description,
				linkToLoginView);
		formLayout.setColspan(email, 2);
		bindFormFields();
		add(title, description, linkToLoginView, formLayout, buttonsLayout);

	}

	private void resetRegistrationForm() {
		binder.setBean(new Account());
	}

	private void saveNewAccount() {
		if (!binder.validate().hasErrors()) {
			Account newAccount = binder.getBean();
			accountService.saveAccount(newAccount);
			UI.getCurrent().navigate(LoginView.class);
		}
	}

	private void bindFormFields() {
		binder.forField(userName)
				.withValidator(name -> name.length() <= 64 && name.length() > 1,
						"Name is not the correct length (Max 64/Min 1)")
				.asRequired().bind(Account::getUserName, Account::setUserName);
		binder.forField(password)
				.withValidator(pass -> pass.length() <= 16 && pass.length() > 1,
						"Password is not the correct length (Max 16/Min 1)")
				.asRequired().bind(Account::getPassword, Account::setPassword);
		binder.forField(gender).bind(Account::getGender, Account::setGender);
		binder.forField(email).withValidator(new EmailValidator("This is not a valid email"))
				.withValidator(mail -> mail.contains("@"), "Email does not contain @ sign").asRequired()
				.bind(Account::getEmail, Account::setEmail);
		binder.forField(birthDate).bind(Account::getBirthDate, Account::setBirthDate);
		binder.setBean(new Account());

	}

}
