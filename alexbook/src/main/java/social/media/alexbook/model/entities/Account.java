package social.media.alexbook.model.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import social.media.alexbook.enums.Gender;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "user_name", nullable = false, unique = true, length = 64)
	private String userName;
	@Column(name = "password", nullable = false, unique = false, length = 16)
	private String password;
	@Column(name = "email", nullable = false, unique = true, length = 64)
	private String email;
	@Column(name = "birth_date", nullable = true, unique = false)
	private LocalDate birthDate;
	@Column(name = "gender", nullable = true)
	private Gender gender;

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public String getEmail() {
		return email;
	}

	public Gender getGender() {
		return gender;
	}

	public Long getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public String getUserName() {
		return userName;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", userName=" + userName + ", password=" + password + ", email=" + email
				+ ", birthDate=" + birthDate + ", gender=" + gender + "]";
	}

}
