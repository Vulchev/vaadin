package social.media.alexbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlexbookApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlexbookApplication.class, args);
	}

}
