package social.media.alexbook;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("social.media.alexbook")
@Configuration
public class ApplicationConfiguration {

}
