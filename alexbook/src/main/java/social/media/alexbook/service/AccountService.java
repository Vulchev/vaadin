package social.media.alexbook.service;

import social.media.alexbook.model.entities.Account;

public interface AccountService {

	void saveAccount(Account newAccount);

	String retrievePass(String email);

	Account checkUserCredentials(String username, String password);

}
