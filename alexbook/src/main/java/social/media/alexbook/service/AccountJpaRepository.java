package social.media.alexbook.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import social.media.alexbook.model.entities.Account;

@Repository
public interface AccountJpaRepository extends JpaRepository<Account, Long> {

	@Query(value = "select a.password from Account a where a.email = :email")
	String findPasswordByEmail(@Param("email") String email);

	Account findByUserNameAndPassword(String username, String password);

}
