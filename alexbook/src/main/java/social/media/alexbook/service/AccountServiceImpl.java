package social.media.alexbook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vaadin.flow.spring.annotation.VaadinSessionScope;

import social.media.alexbook.model.entities.Account;

@Service
@VaadinSessionScope
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountJpaRepository accountJpaRepository;

	@Override
	public void saveAccount(Account newAccount) {
		accountJpaRepository.save(newAccount);
	}

	@Override
	public String retrievePass(String email) {
		return accountJpaRepository.findPasswordByEmail(email);
	}

	@Override
	public Account checkUserCredentials(String username, String password) {
		return accountJpaRepository.findByUserNameAndPassword(username, password);
	}

}
