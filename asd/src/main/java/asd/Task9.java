package asd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Task9 {

	public static void main(String[] args) {

		List<Student> studentsList = new ArrayList<Student>();

		Student maria = new Student(25, "Maria");
		Student alex = new Student(19, "Alex");
		Student naiden = new Student(21, "Naiden");
		Student sonq = new Student(50, "Sonq");
		Student hector = new Student(8, "Hector");

		studentsList.addAll(Arrays.asList(maria, alex, naiden, sonq, hector));

		System.out.println("Array before the switch of element 3 and element 5: " + studentsList.toString());
		Collections.swap(studentsList, 2, 4);
		System.out.println("Array after the switch of element 3 and element 5: " + studentsList.toString());

	}

}
