package asd;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Task15 {

	public static void main(String[] args) {
		List<String> chemistryClass = new LinkedList<String>();

		chemistryClass.add("Alex");
		chemistryClass.add("Mitko");
		chemistryClass.add("Sonq");
		chemistryClass.add("Alex");
		chemistryClass.add("Sonq");
		chemistryClass.add("Sonq");
		chemistryClass.add("Hecti");

		Map<String, Integer> nameOccurances = new TreeMap<>();

		for (String name : chemistryClass) {
			if (nameOccurances.containsKey(name)) {
				nameOccurances.put(name, nameOccurances.get(name) + 1);
			} else {
				nameOccurances.put(name, 1);
			}

		}

		System.out.print(nameOccurances.toString());

	}

}
