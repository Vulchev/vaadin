package asd;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task18 {

	public static void main(String[] args) {

		Set<String> mathClass = new TreeSet<String>();
		Set<String> chemistryClass = new TreeSet<String>();

		mathClass.add("Alex");
		mathClass.add("Mitko");
		mathClass.add("Sonq");
		mathClass.add("Vlado");

		chemistryClass.add("Vlado");
		chemistryClass.add("Mitko");
		chemistryClass.add("Ivan");
		chemistryClass.add("Pafi");

		Set<String> combinedSet = Stream.concat(mathClass.stream(), chemistryClass.stream())
				.collect(Collectors.toSet());

		System.out.print(combinedSet.toString());

		Set<String> combinedFromFirst = new HashSet<>(mathClass);

		System.out.println();
		System.out.print(combinedFromFirst.toString());

		Set<String> combinedFromTwoDifferent = new TreeSet<>();
		Set<String> duplicateOfMathClass = new TreeSet<>(mathClass);
		mathClass.removeAll(chemistryClass);
		chemistryClass.removeAll(duplicateOfMathClass);
		combinedFromTwoDifferent.addAll(mathClass);
		combinedFromTwoDifferent.addAll(chemistryClass);
		System.out.println();
		System.out.print(combinedFromTwoDifferent);

	}

}
