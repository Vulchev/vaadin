package asd;

import java.util.ArrayList;
import java.util.List;

public class Task13 {

	public static void main(String[] args) {

		List<Student> mathClass = new ArrayList<Student>();
		List<Student> chemistryClass = new ArrayList<Student>();

		Student maria = new Student(25, "Maria");
		Student alex = new Student(19, "Alex");
		Student suni = new Student(5, "Suni");
		Student svenja = new Student(20, "Svenja");
		Student simeon = new Student(25, "Simeon");

		mathClass.add(maria);
		mathClass.add(alex);

		chemistryClass.add(suni);
		chemistryClass.add(svenja);
		chemistryClass.add(simeon);

		mathClass.forEach(s -> System.out.println(s.getAge() + " name " + s.getName()));
		chemistryClass.forEach(s -> System.out.println(s.getAge() + " name " + s.getName()));

	}

}
