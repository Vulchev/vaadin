package asd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task10 {

	public static void main(String[] args) {
		List<Student> studentsList = new ArrayList<Student>();

		Student maria = new Student(25, "Maria");
		Student alex = new Student(19, "Alex");
		Student naiden = new Student(21, "Naiden");
		Student sonq = new Student(50, "Sonq");
		Student hector = new Student(8, "Hector");
		Student suni = new Student(5, "Suni");
		Student svenja = new Student(20, "Svenja");
		Student simeon = new Student(25, "Simeon");
		Student mitko = new Student(34, "Dimitar");

		studentsList.addAll(Arrays.asList(maria, alex, naiden, sonq, hector, suni, svenja, simeon, mitko));
		List<Student> cutList = studentsList.subList(3, 8);
		System.out.println(cutList);

	}

}
