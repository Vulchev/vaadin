package asd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Task14 {

	public static void main(String[] args) {
		List<String> mathClass = new ArrayList<String>();
		List<String> chemistryClass = new LinkedList<String>();

		mathClass.add("Alex");
		mathClass.add("Mitko");
		mathClass.add("Sonq");

		chemistryClass.add("Vlado");
		chemistryClass.add("Ivan");
		chemistryClass.add("Pafi");

		mathClass.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);

		Collections.reverse(chemistryClass);
		System.out.println("------------------");
		for (String name : chemistryClass) {
			System.out.println(name);
		}

	}

}
