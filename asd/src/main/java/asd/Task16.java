package asd;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Task16 {

	public static void main(String[] args) {
		List<String> mathClass = new ArrayList<String>();
		List<String> chemistryClass = new LinkedList<String>();

		mathClass.add("Alex");
		mathClass.add("Mitko");
		mathClass.add("Sonq");

		chemistryClass.add("Vlado");
		chemistryClass.add("Ivan");
		chemistryClass.add("Pafi");

		System.out.println("are the two list equal : " + mathClass.containsAll(chemistryClass));

	}

}
