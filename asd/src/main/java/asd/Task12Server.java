package asd;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Task12Server {

	public static final int PORT = 1905;

	public static void main(String[] args) {
		try {
			ServerSocket server = new ServerSocket(PORT);
			System.out.println("Server is running on port: " + PORT);
			Socket socket = server.accept();
			ObjectInputStream inStream = new ObjectInputStream(socket.getInputStream());

			Student receivedStudent = (Student) inStream.readObject();
			System.out.println(receivedStudent.toString());
			ObjectOutputStream outStream = new ObjectOutputStream(
					new FileOutputStream(new File("C:\\Users\\Alexander\\Desktop\\test\\students.txt")));
			outStream.writeObject(receivedStudent);
			server.close();
			socket.close();
			inStream.close();
			outStream.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
