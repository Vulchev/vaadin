package asd;

import java.util.HashSet;
import java.util.Set;

public class Task17 {

	public static void main(String[] args) {

		Set<Student> studentSet = new HashSet<>();
		Student pesho1 = new Student(20, "Pesho");
		Student pesho2 = new Student(21, "Pesho");
		Student gosho = new Student(21, "Gosho");
		Student pesho3 = new Student(20, "Pesho");

		studentSet.add(pesho1);
		studentSet.add(pesho2);
		studentSet.add(gosho);
		studentSet.add(pesho3);

		studentSet.forEach(s -> System.out.println(s.getAge() + " name " + s.getName()));
	}

}
