package asd;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Task12Client {

	public static void main(String[] args) {
		try {
			Socket socket = new Socket("127.0.0.1", Task12Server.PORT);

			ObjectOutputStream outStream = new ObjectOutputStream(socket.getOutputStream());

			outStream.writeObject(new Student(22, "Simona"));
			socket.close();
			outStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
