package asd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class CopyFIler {

	public static void main(String[] args) {
		try {
			copyFileUsingStream(new File("C:\\Users\\Alexander\\Desktop\\test\\testDirText.txt"),
					new File("C:\\Users\\Alexander\\Desktop\\test\\New folder\\testDirText.txt"));
			copyFileUsingChannel(new File("C:\\Users\\Alexander\\Desktop\\test\\testDirText.txt"),
					new File("C:\\Users\\Alexander\\Desktop\\test\\New folder\\testDirText.txt"));
			copyFileUsingJava7Files(new File("C:\\Users\\Alexander\\Desktop\\test\\testDirText.txt"),
					new File("C:\\Users\\Alexander\\Desktop\\test\\New folder\\test.txt"));
			deleteFile(Paths.get("C:\\Users\\Alexander\\Desktop\\test\\New folder\\testDirText.txt"));
			cutFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void cutFile() throws IOException {
		Files.move(Paths.get("C:\\Users\\Alexander\\Desktop\\test\\testDirText.txt"),
				Paths.get("C:\\Users\\Alexander\\Desktop\\test\\New folder\\testDirText.txt"),
				StandardCopyOption.REPLACE_EXISTING);
	}

	public static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}

	public static void copyFileUsingChannel(File source, File dest) throws IOException {
		FileChannel sourceChannel = null;
		FileChannel destChannel = null;
		try {
			sourceChannel = new FileInputStream(source).getChannel();
			destChannel = new FileOutputStream(dest).getChannel();
			destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
		} finally {
			sourceChannel.close();
			destChannel.close();
		}
	}

	public static void copyFileUsingJava7Files(File source, File dest) throws IOException {
		Files.copy(source.toPath(), dest.toPath());
	}

	public static void deleteFile(Path source) {
		try {
			Files.delete(source);
		} catch (NoSuchFileException x) {
			System.err.format("%s: no such" + " file or directory%n", source);
		} catch (DirectoryNotEmptyException x) {
			System.err.format("%s not empty%n", source);
		} catch (IOException x) {
			System.err.println(x);
		}
	}

}
