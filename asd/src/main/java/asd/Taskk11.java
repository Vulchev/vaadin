package asd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Taskk11 {

	public static void main(String[] args) {
		List<Student> mathClass = new ArrayList<Student>();
		List<Student> chemistryClass = new ArrayList<Student>();

		Student maria = new Student(25, "Maria");
		Student alex = new Student(19, "Alex");
		Student naiden = new Student(21, "Naiden");
		Student sonq = new Student(50, "Sonq");
		Student hector = new Student(8, "Hector");
		Student suni = new Student(5, "Suni");
		Student svenja = new Student(20, "Svenja");
		Student simeon = new Student(25, "Simeon");
		Student mitko = new Student(34, "Dimitar");

		mathClass.add(maria);
		mathClass.add(alex);
		mathClass.add(naiden);
		mathClass.add(sonq);
		mathClass.add(hector);

		chemistryClass.add(suni);
		chemistryClass.add(svenja);
		chemistryClass.add(simeon);
		chemistryClass.add(mitko);

		System.out.println(mathClass.toString());
		System.out.println(chemistryClass.toString());

		List<Student> wholeClass = Stream.concat(mathClass.stream(), chemistryClass.stream())
				.collect(Collectors.toList());

		System.out.println(wholeClass.toString());

	}

}
